package DateEnumsanLists;

public enum WnameLibra {

	 Val(1,"Vallhala"), Asg(2,"Asguard"), Cyr(3,"Cyrodill"); 
	

	 private final int IDN;
	 private final String value;
	 private WnameLibra(int IDN, String value)
	 { this.IDN = IDN;
	 this.value = value; }
	
	 
	 public int getIDN() {
		return IDN;
	}
	public String getValue() {
		return value;
	}
	 
	 
}
