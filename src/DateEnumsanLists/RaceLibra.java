package DateEnumsanLists;

public enum RaceLibra {
 Human(1,"Human"), Orc(2,"Orc"), Nephelim(3,"Nephelim"), 
 Elf(4,"Elf"), Dwarf(5,"Dwarf"), Gnome(6,"Gnome");

 private final int IDN;
 private final String value;
 private RaceLibra(int IDN, String value)
 { this.IDN = IDN;
 this.value = value; }

 public int getIDN() {
	return IDN;
}
public String getValue() {
	return value;
}

 
}


