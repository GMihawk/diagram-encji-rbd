package DateEnumsanLists;

public enum QtypeLibra {

	LF(1,"lookfor"), ELL(2,"ellimination"), HUN(3,"hunt"),
	SC(4,"scout"), GATH(5,"gathering"), DUEL(6,"duel"),
	TRA(7,"training"), PER(8,"persuasion");	

	 private final int IDN;
	 private final String value;
	 private QtypeLibra(int IDN, String value)
	 { this.IDN = IDN;
	 this.value = value; }
	
	 
	 public int getIDN() {
		return IDN;
	}
	public String getValue() {
		return value;
	}


}
