package DateEnumsanLists;

public enum NrelationLibra {

	 GOOD(1,"Good"), NEU(2,"Neutral"), ENE(3,"Enemy"); 
		

	 private final int IDN;
	 private final String value;
	 private NrelationLibra(int IDN, String value)
	 { this.IDN = IDN;
	 this.value = value; }
	
	 
	 public int getIDN() {
		return IDN;
	}
	public String getValue() {
		return value;
	}
}
