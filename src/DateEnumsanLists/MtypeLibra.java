package DateEnumsanLists;

public enum MtypeLibra {

	 COM(1,"Common"), ELI(2,"Elite"), HE(3,"Higher Elite"), 
	 LEG(4, "Legendary"); 
		

	 private final int IDN;
	 private final String value;
	 private MtypeLibra(int IDN, String value)
	 { this.IDN = IDN;
	 this.value = value; }
	
	 public int getIDN() {
		return IDN;
	}
	public String getValue() {
		return value;
	}
	 
	 
}
