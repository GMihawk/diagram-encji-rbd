package DateEnumsanLists;

public enum ItypeLibra {

	 CON(1,"Container"), ALCH(2,"Alchemy"), QuM(3,"Quartermaster"),
	 WEA(4,"Weapon"), ARM(5,"Armor"), AMU(6,"Ammunition"),
	 BOO(7,"Books"), ANI(8,"AnimalParts"), FIN(9,"Finding"); 
		

	 private final int IDN;
	 private final String value;
	 private ItypeLibra(int IDN, String value)
	 { this.IDN = IDN;
	 this.value = value; }
	
	 public int getIDN() {
		return IDN;
	}
	public String getValue() {
		return value;
	}
	 
	 
}
