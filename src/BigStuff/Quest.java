package BigStuff;

public class Quest {
	//TODO po co  w nazwie atrybutu? zmienic na zwyk�e name ...
	private String Qname;
	private String Qtype;
	private String QstartNPC;
	private String QfinishNPC;
	private float Qexpirience;
	private int Qreppoints;
	private float Qcurrency;
	
public Quest(String qname, String qtype, String qstartNPC,
			String qfinishNPC, float qexpirience, int qreppoints,
			float qcurrency) {
		super();
		Qname = qname;
			Qtype = qtype;
		QstartNPC = qstartNPC;
			QfinishNPC = qfinishNPC;
		Qexpirience = qexpirience;
			Qreppoints = qreppoints;
		Qcurrency = qcurrency;
	}
	//-----------------------------------------------------	
	public String getQname() {
		return Qname;
	}
	public void setQname(String qname) {
		Qname = qname;
	}
	public String getQtype() {
		return Qtype;
	}
	public void setQtype(String qtype) {
		Qtype = qtype;
	}
	public String getQstartNPC() {
		return QstartNPC;
	}
	public void setQstartNPC(String qstartNPC) {
		QstartNPC = qstartNPC;
	}
	public String getQfinishNPC() {
		return QfinishNPC;
	}
	public void setQfinishNPC(String qfinishNPC) {
		QfinishNPC = qfinishNPC;
	}
	public float getQexpirience() {
		return Qexpirience;
	}
	public void setQexpirience(float qexpirience) {
		Qexpirience = qexpirience;
	}
	public int getQreppoints() {
		return Qreppoints;
	}
	public void setQreppoints(int qreppoints) {
		Qreppoints = qreppoints;
	}
	public float getQcurrency() {
		return Qcurrency;
	}
	public void setQcurrency(float qcurrency) {
		Qcurrency = qcurrency;
	}
	
	
}
