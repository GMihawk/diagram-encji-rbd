package BigStuff;

public class Items {
	//TODO po co  w nazwie atrybutu? zmienic na zwyk�e name ...
	private String Iname;
	private String Itype;
	private String Idescription;
	private String Ieffect;
	private int Iquantity;
	private float Ivalueingold;
	
public Items(String iname, String itype, String idescription,
			String ieffect, int iquantity, float ivalueingold) {
		super();
		Iname = iname;
			Itype = itype;
		Idescription = idescription;
			Ieffect = ieffect;
		Iquantity = iquantity;
			Ivalueingold = ivalueingold;
	}
	//-----------------------------------------
	public String getIname() {
		return Iname;
	}
	public void setIname(String iname) {
		Iname = iname;
	}
	public String getItype() {
		return Itype;
	}
	public void setItype(String itype) {
		Itype = itype;
	}
	public String getIdescription() {
		return Idescription;
	}
	public void setIdescription(String idescription) {
		Idescription = idescription;
	}
	public String getIeffect() {
		return Ieffect;
	}
	public void setIeffect(String ieffect) {
		Ieffect = ieffect;
	}
	public int getIquantity() {
		return Iquantity;
	}
	public void setIquantity(int iquantity) {
		Iquantity = iquantity;
	}
	public float getIvalueingold() {
		return Ivalueingold;
	}
	public void setIvalueingold(float ivalueingold) {
		Ivalueingold = ivalueingold;
	}
	
	
}
